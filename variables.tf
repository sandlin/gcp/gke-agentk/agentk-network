variable "project" {
  description = "The GCP Project"
  type = string
}

variable "region" {
  description = "The GCP Region"
  type = string
}

variable "zone" {
  description = "The GCP Zone"
  type = string
}

variable "subnet_cidr" {
  description = "Subnet IP Range"
  type = string
}

variable "prefix" {
  description = "Prefix for names."
  type = string
}