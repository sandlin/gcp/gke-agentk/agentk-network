/*
* Create Network for our TF cluster.
*/
# Store state in the project.
terraform {
  required_version = ">=1.0.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/35449641/terraform/state/groot"
    lock_address   = "https://gitlab.com/api/v4/projects/35449641/terraform/state/groot/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/35449641/terraform/state/groot/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

provider "google" {
  project     = var.project
}


resource "random_pet" "name" {
  length = 1
}

locals {
  prefix = "${var.prefix}-${random_pet.name.id}"
}

// Create VPC
resource "google_compute_network" "vpc" {
 name                    = "${local.prefix}-vpc"
 auto_create_subnetworks = "false"
}

// Create Subnet
resource "google_compute_subnetwork" "subnet" {
 name          = "${local.prefix}-subnet"
 ip_cidr_range = var.subnet_cidr
 network       = "${local.prefix}-vpc"
 depends_on    = [google_compute_network.vpc]
 region      = var.region
}

// VPC firewall configuration
resource "google_compute_firewall" "firewall" {
  name    = "${local.prefix}-firewall"
  network = "${google_compute_network.vpc.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  depends_on    = [google_compute_network.vpc]
}
